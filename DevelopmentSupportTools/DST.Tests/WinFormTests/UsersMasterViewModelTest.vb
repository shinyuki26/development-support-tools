﻿Imports DST.Domain.Entities
Imports DST.Domain.Repositories
Imports DST.Tests.TestData
Imports DST.WinForm.ViewModels
Imports Moq

<TestClass()> Public Class UsersMasterViewModelTest

    <TestClass()> Public Class 初期表示

        Private _target As UsersMasterViewModel

        <TestInitialize> Public Sub 前準備()
            Dim usersMock As New Mock(Of IUsersRepository)
            _target = New UsersMasterViewModel(usersMock.Object)
        End Sub

        <TestMethod()> Public Sub 名前テキストが空であること()
            Assert.AreEqual("", _target.NameText)
        End Sub

        <TestMethod()> Public Sub ユーザーグリッドが0件であること()
            Assert.AreEqual(0, _target.UsersDataSource.Count)
        End Sub

    End Class

    <TestClass()> Public Class 検索ボタンクリック
        Private _target As UsersMasterViewModel

        <TestInitialize> Public Sub 前準備()
            Dim usersMock As New Mock(Of IUsersRepository)
            usersMock.Setup(Function(x) x.GetUsersByName("山田", True)).Returns(UsersTestData.YamadaBrothers)
            _target = New UsersMasterViewModel(usersMock.Object)
        End Sub

        <TestMethod()> Public Sub ユーザーグリッドにデータが設定されること()
            Dim expected As List(Of UserEntity) = UsersTestData.YamadaBrothers

            _target.NameText = "山田"
            _target.Search()

            Assert.AreEqual(expected.Count, _target.UsersDataSource.Count)

            Dim rowIndex As Integer = 0
            For Each actual In _target.UsersDataSource
                Assert.AreEqual(expected(rowIndex).UserId, actual.UserId)
                Assert.AreEqual(expected(rowIndex).FamilyName, actual.FamilyName)
                Assert.AreEqual(expected(rowIndex).GivenName, actual.GivenName)
                Assert.AreEqual(expected(rowIndex).IsActive, actual.IsActive)
                Assert.AreEqual(expected(rowIndex).CreatedBy, actual.CreatedBy)
                Assert.AreEqual(expected(rowIndex).CreatedOn, actual.CreatedOn)
                Assert.AreEqual(expected(rowIndex).UpdatedBy, actual.UpdatedBy)
                Assert.AreEqual(expected(rowIndex).UpdatedOn, actual.UpdatedOn)
                rowIndex += 1
            Next
        End Sub

    End Class

End Class