﻿Imports DST.Domain
Imports DST.Domain.Entities
Imports DST.Domain.Exceptions
Imports DST.Domain.Repositories
Imports DST.Tests.TestData
Imports DST.WinForm.ViewModels
Imports Moq

<TestClass()> Public Class LoginViewModelTest

    <TestClass()> Public Class 初期表示
        Private _target As LoginViewModel

        <TestInitialize> Public Sub 前準備()
            Dim usersMock As New Mock(Of IUsersRepository)
            _target = New LoginViewModel(usersMock.Object)
        End Sub

        <TestMethod()> Public Sub ユーザIDテキストが空であること()
            Assert.AreEqual("", _target.UserIdText)
        End Sub

        <TestMethod()> Public Sub パスワードテキストが空であること()
            Assert.AreEqual("", _target.PasswordText)
        End Sub

    End Class

    <TestClass()> Public Class ログインボタン押下時
        Private _target As LoginViewModel

        <TestInitialize> Public Sub 前準備()
            Dim usersMock As New Mock(Of IUsersRepository)
            usersMock.Setup(Function(x) x.GetUserByKey(1)).Returns(UsersTestData.YamadaIchiro)

            _target = New LoginViewModel(usersMock.Object)
        End Sub

        <TestMethod()> Public Sub ユーザIDが空の場合エラーとなること()
            _target.UserIdText = ""
            Dim ex As InputException = Assert.ThrowsException(Of InputException)(Sub() _target.Login())
            Assert.AreEqual("ユーザIDを入力してください。", ex.Message)
        End Sub

        <TestMethod()> Public Sub ユーザIDが数値でない場合エラーとなること()
            _target.UserIdText = "NotInteger"
            Dim ex As InputException = Assert.ThrowsException(Of InputException)(Sub() _target.Login())
            Assert.AreEqual("ユーザIDは数値を入力してください。", ex.Message)
        End Sub

        <TestMethod()> Public Sub ユーザIDがユーザマスタデータに存在しない場合エラーとなること()
            _target.UserIdText = "99999"
            Dim ex As InputException = Assert.ThrowsException(Of InputException)(Sub() _target.Login())
            Assert.AreEqual("ユーザIDが利用可能ユーザとして登録されていません。", ex.Message)
        End Sub

        <TestMethod()> Public Sub ユーザIDがユーザマスタデータに存在する場合共有変数ログインユーザとして保持されること()
            _target.UserIdText = "1"
            _target.Login()
            Dim expected As UserEntity = UsersTestData.YamadaIchiro
            Dim actual As UserEntity = [Shared].LoginUser
            Assert.AreEqual(expected.UserId, actual.UserId)
            Assert.AreEqual(expected.FamilyName, actual.FamilyName)
            Assert.AreEqual(expected.GivenName, actual.GivenName)
            Assert.AreEqual(expected.IsActive, actual.IsActive)
            Assert.AreEqual(expected.CreatedBy, actual.CreatedBy)
            Assert.AreEqual(expected.CreatedOn, actual.CreatedOn)
            Assert.AreEqual(expected.UpdatedBy, actual.UpdatedBy)
            Assert.AreEqual(expected.UpdatedOn, actual.UpdatedOn)
        End Sub

    End Class

End Class