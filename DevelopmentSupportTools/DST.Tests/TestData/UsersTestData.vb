﻿Imports DST.Domain.Entities

Namespace TestData

    Friend Class UsersTestData

        Public Shared ReadOnly Property YamadaBrothers As List(Of UserEntity)
            Get
                Dim users As New List(Of UserEntity) From {
                    New UserEntity With {
                        .UserId = 1, .FamilyName = "山田", .GivenName = "一郎", .IsActive = True,
                        .CreatedBy = 1, .CreatedOn = New Date(2020, 1, 1), .UpdatedBy = 1, .UpdatedOn = New Date(2020, 1, 1)
                    },
                    New UserEntity With {
                        .UserId = 2, .FamilyName = "山田", .GivenName = "二郎", .IsActive = True,
                        .CreatedBy = 1, .CreatedOn = New Date(2020, 2, 2), .UpdatedBy = 1, .UpdatedOn = New Date(2020, 2, 2)
                    },
                    New UserEntity With {
                        .UserId = 3, .FamilyName = "山田", .GivenName = "三郎", .IsActive = False,
                        .CreatedBy = 1, .CreatedOn = New Date(2020, 3, 3), .UpdatedBy = 2, .UpdatedOn = New Date(2020, 3, 30)
                    }
                }
                Return users
            End Get
        End Property

        Public Shared ReadOnly Property YamadaIchiro As UserEntity
            Get
                Return New UserEntity With {
                    .UserId = 1, .FamilyName = "山田", .GivenName = "一郎", .IsActive = True,
                    .CreatedBy = 1, .CreatedOn = New Date(2020, 1, 1), .UpdatedBy = 1, .UpdatedOn = New Date(2020, 1, 1)
                }
            End Get
        End Property

    End Class

End Namespace