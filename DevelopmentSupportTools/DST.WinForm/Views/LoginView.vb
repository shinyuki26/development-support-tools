﻿Imports DST.Infrastructure.Oracle
Imports DST.WinForm.ViewModels

Namespace Views

    ''' <summary>
    ''' ログイン画面
    ''' </summary>
    Public Class LoginView
        Private ReadOnly _viewModel As New LoginViewModel(New UsersOracle(New FreeContext))

        ''' <summary>
        ''' コンストラクタ
        ''' </summary>
        Public Sub New()

            ' この呼び出しはデザイナーで必要です。
            InitializeComponent()

            ' InitializeComponent() 呼び出しの後で初期化を追加します。
            LoginViewModelBindingSource.DataSource = _viewModel
        End Sub

        ''' <summary>
        ''' ログインボタンクリックイベント
        ''' </summary>
        ''' <param name="sender">コントロール</param>
        ''' <param name="e">イベント引数</param>
        Private Sub LoginButton_Click(sender As Object, e As EventArgs) Handles LoginButton.Click
            Try
                _viewModel.Login()
                Hide()
                Using view As New MenuView
                    view.ShowDialog()
                End Using
                Close()
            Catch ex As Exception
                HandleException(ex)
            End Try
        End Sub

    End Class

End Namespace