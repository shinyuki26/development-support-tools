﻿Namespace Views

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
    Partial Class UsersMasterView
        Inherits WinForm.Views.AbstractListDetailView

        'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
        <System.Diagnostics.DebuggerNonUserCode()>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

        'Windows フォーム デザイナーで必要です。
        Private components As System.ComponentModel.IContainer

        'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
        'Windows フォーム デザイナーを使用して変更できます。  
        'コード エディターを使って変更しないでください。
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Me.UsersMasterViewModelBindingSource = New System.Windows.Forms.BindingSource(Me.components)
            Me.NameTextMaskedTextBox = New System.Windows.Forms.MaskedTextBox()
            Me.NameLabel = New MetroFramework.Controls.MetroLabel()
            Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
            Me.UpdatedOnMetroLabel = New MetroFramework.Controls.MetroLabel()
            Me.UsersBindingSource = New System.Windows.Forms.BindingSource(Me.components)
            Me.UpdatedByMetroLabel = New MetroFramework.Controls.MetroLabel()
            Me.CreatedOnMetroLabel = New MetroFramework.Controls.MetroLabel()
            Me.CreatedByMetroLabel = New MetroFramework.Controls.MetroLabel()
            Me.IsActiveMetroToggle = New MetroFramework.Controls.MetroToggle()
            Me.GivenNameTextBox = New System.Windows.Forms.TextBox()
            Me.FamilyNameTextBox = New System.Windows.Forms.TextBox()
            Me.UserIdMetroLabel = New MetroFramework.Controls.MetroLabel()
            Me.UserIdLabel = New MetroFramework.Controls.MetroLabel()
            Me.FamilyNameLabel = New MetroFramework.Controls.MetroLabel()
            Me.GivenNameLabel = New MetroFramework.Controls.MetroLabel()
            Me.IsActiveLabel = New MetroFramework.Controls.MetroLabel()
            Me.CreatedByLabel = New MetroFramework.Controls.MetroLabel()
            Me.CreatedOnLabel = New MetroFramework.Controls.MetroLabel()
            Me.UpdatedByLabel = New MetroFramework.Controls.MetroLabel()
            Me.UpdatedOnLabel = New MetroFramework.Controls.MetroLabel()
            CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SplitContainer1.Panel1.SuspendLayout()
            Me.SplitContainer1.Panel2.SuspendLayout()
            Me.SplitContainer1.SuspendLayout()
            CType(Me.SplitContainer2, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SplitContainer2.Panel2.SuspendLayout()
            Me.SplitContainer2.SuspendLayout()
            CType(Me.UsersMasterViewModelBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.TableLayoutPanel1.SuspendLayout()
            CType(Me.UsersBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'SplitContainer1
            '
            '
            'SplitContainer1.Panel1
            '
            Me.SplitContainer1.Panel1.Controls.Add(Me.NameLabel)
            Me.SplitContainer1.Panel1.Controls.Add(Me.NameTextMaskedTextBox)
            '
            'SplitContainer2
            '
            '
            'SplitContainer2.Panel2
            '
            Me.SplitContainer2.Panel2.Controls.Add(Me.TableLayoutPanel1)
            Me.SplitContainer2.Size = New System.Drawing.Size(1160, 633)
            '
            'SearchButton
            '
            Me.SearchButton.Location = New System.Drawing.Point(210, 33)
            Me.SearchButton.TabIndex = 2
            '
            'AddRowButton
            '
            Me.AddRowButton.Location = New System.Drawing.Point(291, 33)
            Me.AddRowButton.TabIndex = 3
            '
            'SaveButton
            '
            Me.SaveButton.Location = New System.Drawing.Point(372, 33)
            Me.SaveButton.TabIndex = 4
            '
            'UsersMasterViewModelBindingSource
            '
            Me.UsersMasterViewModelBindingSource.DataSource = GetType(DST.WinForm.ViewModels.UsersMasterViewModel)
            '
            'NameTextMaskedTextBox
            '
            Me.NameTextMaskedTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.UsersMasterViewModelBindingSource, "NameText", True))
            Me.NameTextMaskedTextBox.Location = New System.Drawing.Point(81, 35)
            Me.NameTextMaskedTextBox.Name = "NameTextMaskedTextBox"
            Me.NameTextMaskedTextBox.Size = New System.Drawing.Size(100, 19)
            Me.NameTextMaskedTextBox.TabIndex = 1
            '
            'NameLabel
            '
            Me.NameLabel.AutoSize = True
            Me.NameLabel.Location = New System.Drawing.Point(24, 35)
            Me.NameLabel.Name = "NameLabel"
            Me.NameLabel.Size = New System.Drawing.Size(51, 19)
            Me.NameLabel.TabIndex = 4
            Me.NameLabel.Text = "名前："
            '
            'TableLayoutPanel1
            '
            Me.TableLayoutPanel1.ColumnCount = 2
            Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
            Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
            Me.TableLayoutPanel1.Controls.Add(Me.UpdatedOnLabel, 0, 7)
            Me.TableLayoutPanel1.Controls.Add(Me.IsActiveLabel, 0, 3)
            Me.TableLayoutPanel1.Controls.Add(Me.UpdatedByLabel, 0, 6)
            Me.TableLayoutPanel1.Controls.Add(Me.GivenNameLabel, 0, 2)
            Me.TableLayoutPanel1.Controls.Add(Me.CreatedOnLabel, 0, 5)
            Me.TableLayoutPanel1.Controls.Add(Me.FamilyNameLabel, 0, 1)
            Me.TableLayoutPanel1.Controls.Add(Me.CreatedByLabel, 0, 4)
            Me.TableLayoutPanel1.Controls.Add(Me.UpdatedOnMetroLabel, 1, 7)
            Me.TableLayoutPanel1.Controls.Add(Me.UpdatedByMetroLabel, 1, 6)
            Me.TableLayoutPanel1.Controls.Add(Me.CreatedOnMetroLabel, 1, 5)
            Me.TableLayoutPanel1.Controls.Add(Me.CreatedByMetroLabel, 1, 4)
            Me.TableLayoutPanel1.Controls.Add(Me.IsActiveMetroToggle, 1, 3)
            Me.TableLayoutPanel1.Controls.Add(Me.GivenNameTextBox, 1, 2)
            Me.TableLayoutPanel1.Controls.Add(Me.FamilyNameTextBox, 1, 1)
            Me.TableLayoutPanel1.Controls.Add(Me.UserIdMetroLabel, 1, 0)
            Me.TableLayoutPanel1.Controls.Add(Me.UserIdLabel, 0, 0)
            Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
            Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
            Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
            Me.TableLayoutPanel1.Padding = New System.Windows.Forms.Padding(30)
            Me.TableLayoutPanel1.RowCount = 10
            Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
            Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
            Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
            Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
            Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
            Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
            Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
            Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
            Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
            Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10.0!))
            Me.TableLayoutPanel1.Size = New System.Drawing.Size(345, 631)
            Me.TableLayoutPanel1.TabIndex = 0
            '
            'UpdatedOnMetroLabel
            '
            Me.UpdatedOnMetroLabel.Anchor = System.Windows.Forms.AnchorStyles.Left
            Me.UpdatedOnMetroLabel.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.UsersBindingSource, "UpdatedOn", True))
            Me.UpdatedOnMetroLabel.Location = New System.Drawing.Point(175, 446)
            Me.UpdatedOnMetroLabel.Name = "UpdatedOnMetroLabel"
            Me.UpdatedOnMetroLabel.Size = New System.Drawing.Size(125, 23)
            Me.UpdatedOnMetroLabel.TabIndex = 15
            '
            'UsersBindingSource
            '
            Me.UsersBindingSource.DataMember = "UsersDataSource"
            Me.UsersBindingSource.DataSource = Me.UsersMasterViewModelBindingSource
            '
            'UpdatedByMetroLabel
            '
            Me.UpdatedByMetroLabel.Anchor = System.Windows.Forms.AnchorStyles.Left
            Me.UpdatedByMetroLabel.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.UsersBindingSource, "UpdatedBy", True))
            Me.UpdatedByMetroLabel.Location = New System.Drawing.Point(175, 389)
            Me.UpdatedByMetroLabel.Name = "UpdatedByMetroLabel"
            Me.UpdatedByMetroLabel.Size = New System.Drawing.Size(100, 23)
            Me.UpdatedByMetroLabel.TabIndex = 13
            '
            'CreatedOnMetroLabel
            '
            Me.CreatedOnMetroLabel.Anchor = System.Windows.Forms.AnchorStyles.Left
            Me.CreatedOnMetroLabel.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.UsersBindingSource, "CreatedOn", True))
            Me.CreatedOnMetroLabel.Location = New System.Drawing.Point(175, 332)
            Me.CreatedOnMetroLabel.Name = "CreatedOnMetroLabel"
            Me.CreatedOnMetroLabel.Size = New System.Drawing.Size(125, 23)
            Me.CreatedOnMetroLabel.TabIndex = 11
            '
            'CreatedByMetroLabel
            '
            Me.CreatedByMetroLabel.Anchor = System.Windows.Forms.AnchorStyles.Left
            Me.CreatedByMetroLabel.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.UsersBindingSource, "CreatedBy", True))
            Me.CreatedByMetroLabel.Location = New System.Drawing.Point(175, 275)
            Me.CreatedByMetroLabel.Name = "CreatedByMetroLabel"
            Me.CreatedByMetroLabel.Size = New System.Drawing.Size(100, 23)
            Me.CreatedByMetroLabel.TabIndex = 9
            '
            'IsActiveMetroToggle
            '
            Me.IsActiveMetroToggle.Anchor = System.Windows.Forms.AnchorStyles.Left
            Me.IsActiveMetroToggle.DataBindings.Add(New System.Windows.Forms.Binding("CheckState", Me.UsersBindingSource, "IsActive", True))
            Me.IsActiveMetroToggle.Location = New System.Drawing.Point(175, 217)
            Me.IsActiveMetroToggle.Name = "IsActiveMetroToggle"
            Me.IsActiveMetroToggle.Size = New System.Drawing.Size(104, 24)
            Me.IsActiveMetroToggle.TabIndex = 13
            Me.IsActiveMetroToggle.Text = "Off"
            Me.IsActiveMetroToggle.UseSelectable = True
            '
            'GivenNameTextBox
            '
            Me.GivenNameTextBox.Anchor = System.Windows.Forms.AnchorStyles.Left
            Me.GivenNameTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.UsersBindingSource, "GivenName", True))
            Me.GivenNameTextBox.Location = New System.Drawing.Point(175, 163)
            Me.GivenNameTextBox.Name = "GivenNameTextBox"
            Me.GivenNameTextBox.Size = New System.Drawing.Size(100, 19)
            Me.GivenNameTextBox.TabIndex = 12
            '
            'FamilyNameTextBox
            '
            Me.FamilyNameTextBox.Anchor = System.Windows.Forms.AnchorStyles.Left
            Me.FamilyNameTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.UsersBindingSource, "FamilyName", True))
            Me.FamilyNameTextBox.Location = New System.Drawing.Point(175, 106)
            Me.FamilyNameTextBox.Name = "FamilyNameTextBox"
            Me.FamilyNameTextBox.Size = New System.Drawing.Size(100, 19)
            Me.FamilyNameTextBox.TabIndex = 11
            '
            'UserIdMetroLabel
            '
            Me.UserIdMetroLabel.Anchor = System.Windows.Forms.AnchorStyles.Left
            Me.UserIdMetroLabel.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.UsersBindingSource, "UserId", True))
            Me.UserIdMetroLabel.Location = New System.Drawing.Point(175, 47)
            Me.UserIdMetroLabel.Name = "UserIdMetroLabel"
            Me.UserIdMetroLabel.Size = New System.Drawing.Size(100, 23)
            Me.UserIdMetroLabel.TabIndex = 1
            '
            'UserIdLabel
            '
            Me.UserIdLabel.Anchor = System.Windows.Forms.AnchorStyles.Left
            Me.UserIdLabel.AutoSize = True
            Me.UserIdLabel.Location = New System.Drawing.Point(33, 49)
            Me.UserIdLabel.Name = "UserIdLabel"
            Me.UserIdLabel.Size = New System.Drawing.Size(69, 19)
            Me.UserIdLabel.TabIndex = 5
            Me.UserIdLabel.Text = "ユーザID："
            '
            'FamilyNameLabel
            '
            Me.FamilyNameLabel.Anchor = System.Windows.Forms.AnchorStyles.Left
            Me.FamilyNameLabel.AutoSize = True
            Me.FamilyNameLabel.Location = New System.Drawing.Point(33, 106)
            Me.FamilyNameLabel.Name = "FamilyNameLabel"
            Me.FamilyNameLabel.Size = New System.Drawing.Size(37, 19)
            Me.FamilyNameLabel.TabIndex = 6
            Me.FamilyNameLabel.Text = "姓："
            '
            'GivenNameLabel
            '
            Me.GivenNameLabel.Anchor = System.Windows.Forms.AnchorStyles.Left
            Me.GivenNameLabel.AutoSize = True
            Me.GivenNameLabel.Location = New System.Drawing.Point(33, 163)
            Me.GivenNameLabel.Name = "GivenNameLabel"
            Me.GivenNameLabel.Size = New System.Drawing.Size(37, 19)
            Me.GivenNameLabel.TabIndex = 7
            Me.GivenNameLabel.Text = "名："
            '
            'IsActiveLabel
            '
            Me.IsActiveLabel.Anchor = System.Windows.Forms.AnchorStyles.Left
            Me.IsActiveLabel.AutoSize = True
            Me.IsActiveLabel.Location = New System.Drawing.Point(33, 220)
            Me.IsActiveLabel.Name = "IsActiveLabel"
            Me.IsActiveLabel.Size = New System.Drawing.Size(51, 19)
            Me.IsActiveLabel.TabIndex = 8
            Me.IsActiveLabel.Text = "有効："
            '
            'CreatedByLabel
            '
            Me.CreatedByLabel.Anchor = System.Windows.Forms.AnchorStyles.Left
            Me.CreatedByLabel.AutoSize = True
            Me.CreatedByLabel.Location = New System.Drawing.Point(33, 277)
            Me.CreatedByLabel.Name = "CreatedByLabel"
            Me.CreatedByLabel.Size = New System.Drawing.Size(65, 19)
            Me.CreatedByLabel.TabIndex = 9
            Me.CreatedByLabel.Text = "作成者："
            '
            'CreatedOnLabel
            '
            Me.CreatedOnLabel.Anchor = System.Windows.Forms.AnchorStyles.Left
            Me.CreatedOnLabel.AutoSize = True
            Me.CreatedOnLabel.Location = New System.Drawing.Point(33, 334)
            Me.CreatedOnLabel.Name = "CreatedOnLabel"
            Me.CreatedOnLabel.Size = New System.Drawing.Size(79, 19)
            Me.CreatedOnLabel.TabIndex = 10
            Me.CreatedOnLabel.Text = "作成日時："
            '
            'UpdatedByLabel
            '
            Me.UpdatedByLabel.Anchor = System.Windows.Forms.AnchorStyles.Left
            Me.UpdatedByLabel.AutoSize = True
            Me.UpdatedByLabel.Location = New System.Drawing.Point(33, 391)
            Me.UpdatedByLabel.Name = "UpdatedByLabel"
            Me.UpdatedByLabel.Size = New System.Drawing.Size(65, 19)
            Me.UpdatedByLabel.TabIndex = 11
            Me.UpdatedByLabel.Text = "更新者："
            '
            'UpdatedOnLabel
            '
            Me.UpdatedOnLabel.Anchor = System.Windows.Forms.AnchorStyles.Left
            Me.UpdatedOnLabel.AutoSize = True
            Me.UpdatedOnLabel.Location = New System.Drawing.Point(33, 448)
            Me.UpdatedOnLabel.Name = "UpdatedOnLabel"
            Me.UpdatedOnLabel.Size = New System.Drawing.Size(79, 19)
            Me.UpdatedOnLabel.TabIndex = 12
            Me.UpdatedOnLabel.Text = "更新日時："
            '
            'UsersMasterView
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
            Me.ClientSize = New System.Drawing.Size(1200, 800)
            Me.Name = "UsersMasterView"
            Me.SplitContainer1.Panel1.ResumeLayout(False)
            Me.SplitContainer1.Panel1.PerformLayout()
            Me.SplitContainer1.Panel2.ResumeLayout(False)
            CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
            Me.SplitContainer1.ResumeLayout(False)
            Me.SplitContainer2.Panel2.ResumeLayout(False)
            CType(Me.SplitContainer2, System.ComponentModel.ISupportInitialize).EndInit()
            Me.SplitContainer2.ResumeLayout(False)
            CType(Me.UsersMasterViewModelBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
            Me.TableLayoutPanel1.ResumeLayout(False)
            Me.TableLayoutPanel1.PerformLayout()
            CType(Me.UsersBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)

        End Sub

        Friend WithEvents NameLabel As MetroFramework.Controls.MetroLabel
        Friend WithEvents NameTextMaskedTextBox As MaskedTextBox
        Friend WithEvents UsersMasterViewModelBindingSource As BindingSource
        Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
        Friend WithEvents UpdatedOnMetroLabel As MetroFramework.Controls.MetroLabel
        Friend WithEvents UsersBindingSource As BindingSource
        Friend WithEvents UpdatedByMetroLabel As MetroFramework.Controls.MetroLabel
        Friend WithEvents CreatedOnMetroLabel As MetroFramework.Controls.MetroLabel
        Friend WithEvents CreatedByMetroLabel As MetroFramework.Controls.MetroLabel
        Friend WithEvents IsActiveMetroToggle As MetroFramework.Controls.MetroToggle
        Friend WithEvents GivenNameTextBox As TextBox
        Friend WithEvents FamilyNameTextBox As TextBox
        Friend WithEvents UserIdMetroLabel As MetroFramework.Controls.MetroLabel
        Friend WithEvents UserIdLabel As MetroFramework.Controls.MetroLabel
        Friend WithEvents FamilyNameLabel As MetroFramework.Controls.MetroLabel
        Friend WithEvents GivenNameLabel As MetroFramework.Controls.MetroLabel
        Friend WithEvents UpdatedOnLabel As MetroFramework.Controls.MetroLabel
        Friend WithEvents IsActiveLabel As MetroFramework.Controls.MetroLabel
        Friend WithEvents UpdatedByLabel As MetroFramework.Controls.MetroLabel
        Friend WithEvents CreatedOnLabel As MetroFramework.Controls.MetroLabel
        Friend WithEvents CreatedByLabel As MetroFramework.Controls.MetroLabel
    End Class

End Namespace