﻿Namespace Views

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
    Partial Class LoginView
        Inherits AbstractBaseView

        'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
        <System.Diagnostics.DebuggerNonUserCode()>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Windows フォーム デザイナーで必要です。
        Private components As System.ComponentModel.IContainer

        'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
        'Windows フォーム デザイナーを使用して変更できます。  
        'コード エディターを使って変更しないでください。
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Me.components = New System.ComponentModel.Container()
            Me.PasswordTextMaskedTextBox = New System.Windows.Forms.MaskedTextBox()
            Me.PasswordLabel = New MetroFramework.Controls.MetroLabel()
            Me.UserIdTextBox = New MetroFramework.Controls.MetroTextBox()
            Me.UserIdLabel = New MetroFramework.Controls.MetroLabel()
            Me.LoginButton = New MetroFramework.Controls.MetroButton()
            Me.PictureBox1 = New System.Windows.Forms.PictureBox()
            Me.LoginViewModelBindingSource = New System.Windows.Forms.BindingSource(Me.components)
            CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.LoginViewModelBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
            Me.SuspendLayout()
            '
            'PasswordTextMaskedTextBox
            '
            Me.PasswordTextMaskedTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.LoginViewModelBindingSource, "PasswordText", True))
            Me.PasswordTextMaskedTextBox.Location = New System.Drawing.Point(315, 142)
            Me.PasswordTextMaskedTextBox.Mask = "********"
            Me.PasswordTextMaskedTextBox.Name = "PasswordTextMaskedTextBox"
            Me.PasswordTextMaskedTextBox.Size = New System.Drawing.Size(143, 19)
            Me.PasswordTextMaskedTextBox.TabIndex = 1
            '
            'PasswordLabel
            '
            Me.PasswordLabel.AutoSize = True
            Me.PasswordLabel.Location = New System.Drawing.Point(231, 142)
            Me.PasswordLabel.Name = "PasswordLabel"
            Me.PasswordLabel.Size = New System.Drawing.Size(78, 19)
            Me.PasswordLabel.TabIndex = 5
            Me.PasswordLabel.Text = "パスワード："
            '
            'UserIdTextBox
            '
            '
            '
            '
            Me.UserIdTextBox.CustomButton.Image = Nothing
            Me.UserIdTextBox.CustomButton.Location = New System.Drawing.Point(121, 1)
            Me.UserIdTextBox.CustomButton.Name = ""
            Me.UserIdTextBox.CustomButton.Size = New System.Drawing.Size(21, 21)
            Me.UserIdTextBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue
            Me.UserIdTextBox.CustomButton.TabIndex = 1
            Me.UserIdTextBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light
            Me.UserIdTextBox.CustomButton.UseSelectable = True
            Me.UserIdTextBox.CustomButton.Visible = False
            Me.UserIdTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.LoginViewModelBindingSource, "UserIdText", True))
            Me.UserIdTextBox.Lines = New String() {"MetroTextBox1"}
            Me.UserIdTextBox.Location = New System.Drawing.Point(315, 98)
            Me.UserIdTextBox.MaxLength = 32767
            Me.UserIdTextBox.Name = "UserIdTextBox"
            Me.UserIdTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(0)
            Me.UserIdTextBox.ScrollBars = System.Windows.Forms.ScrollBars.None
            Me.UserIdTextBox.SelectedText = ""
            Me.UserIdTextBox.SelectionLength = 0
            Me.UserIdTextBox.SelectionStart = 0
            Me.UserIdTextBox.ShortcutsEnabled = True
            Me.UserIdTextBox.Size = New System.Drawing.Size(143, 23)
            Me.UserIdTextBox.TabIndex = 0
            Me.UserIdTextBox.Text = "MetroTextBox1"
            Me.UserIdTextBox.UseSelectable = True
            Me.UserIdTextBox.WaterMarkColor = System.Drawing.Color.FromArgb(CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer), CType(CType(109, Byte), Integer))
            Me.UserIdTextBox.WaterMarkFont = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel)
            '
            'UserIdLabel
            '
            Me.UserIdLabel.AutoSize = True
            Me.UserIdLabel.Location = New System.Drawing.Point(231, 98)
            Me.UserIdLabel.Name = "UserIdLabel"
            Me.UserIdLabel.Size = New System.Drawing.Size(69, 19)
            Me.UserIdLabel.TabIndex = 7
            Me.UserIdLabel.Text = "ユーザID："
            '
            'LoginButton
            '
            Me.LoginButton.Location = New System.Drawing.Point(315, 208)
            Me.LoginButton.Name = "LoginButton"
            Me.LoginButton.Size = New System.Drawing.Size(75, 23)
            Me.LoginButton.TabIndex = 3
            Me.LoginButton.Text = "ログイン"
            Me.LoginButton.UseSelectable = True
            '
            'PictureBox1
            '
            Me.PictureBox1.Image = Global.DST.WinForm.My.Resources.Resources.login
            Me.PictureBox1.Location = New System.Drawing.Point(75, 80)
            Me.PictureBox1.Name = "PictureBox1"
            Me.PictureBox1.Size = New System.Drawing.Size(100, 100)
            Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
            Me.PictureBox1.TabIndex = 8
            Me.PictureBox1.TabStop = False
            '
            'LoginViewModelBindingSource
            '
            Me.LoginViewModelBindingSource.DataSource = GetType(DST.WinForm.ViewModels.LoginViewModel)
            '
            'LoginView
            '
            Me.AcceptButton = Me.LoginButton
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(560, 308)
            Me.Controls.Add(Me.PictureBox1)
            Me.Controls.Add(Me.LoginButton)
            Me.Controls.Add(Me.UserIdLabel)
            Me.Controls.Add(Me.UserIdTextBox)
            Me.Controls.Add(Me.PasswordLabel)
            Me.Controls.Add(Me.PasswordTextMaskedTextBox)
            Me.Name = "LoginView"
            Me.Text = "LoginView"
            CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.LoginViewModelBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
            Me.ResumeLayout(False)
            Me.PerformLayout()

        End Sub

        Friend WithEvents LoginViewModelBindingSource As BindingSource
        Friend WithEvents PasswordTextMaskedTextBox As MaskedTextBox
        Friend WithEvents PasswordLabel As MetroFramework.Controls.MetroLabel
        Friend WithEvents UserIdTextBox As MetroFramework.Controls.MetroTextBox
        Friend WithEvents UserIdLabel As MetroFramework.Controls.MetroLabel
        Friend WithEvents LoginButton As MetroFramework.Controls.MetroButton
        Friend WithEvents PictureBox1 As PictureBox
    End Class

End Namespace