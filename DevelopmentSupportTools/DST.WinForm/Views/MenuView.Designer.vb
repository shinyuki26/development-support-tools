﻿Namespace Views

    <Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
    Partial Class MenuView
        Inherits AbstractBaseView

        'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
        <System.Diagnostics.DebuggerNonUserCode()>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            Try
                If disposing AndAlso components IsNot Nothing Then
                    components.Dispose()
                End If
            Finally
                MyBase.Dispose(disposing)
            End Try
        End Sub

        'Windows フォーム デザイナーで必要です。
        Private components As System.ComponentModel.IContainer

        'メモ: 以下のプロシージャは Windows フォーム デザイナーで必要です。
        'Windows フォーム デザイナーを使用して変更できます。  
        'コード エディターを使って変更しないでください。
        <System.Diagnostics.DebuggerStepThrough()>
        Private Sub InitializeComponent()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(MenuView))
            Me.UsersMasterTile = New MetroFramework.Controls.MetroTile()
            Me.SuspendLayout()
            '
            'UsersMasterTile
            '
            Me.UsersMasterTile.ActiveControl = Nothing
            Me.UsersMasterTile.Location = New System.Drawing.Point(84, 115)
            Me.UsersMasterTile.Name = "UsersMasterTile"
            Me.UsersMasterTile.Size = New System.Drawing.Size(120, 120)
            Me.UsersMasterTile.TabIndex = 0
            Me.UsersMasterTile.Text = "ユーザマスタ"
            Me.UsersMasterTile.TextAlign = System.Drawing.ContentAlignment.BottomCenter
            Me.UsersMasterTile.TileImage = CType(resources.GetObject("UsersMasterTile.TileImage"), System.Drawing.Image)
            Me.UsersMasterTile.TileImageAlign = System.Drawing.ContentAlignment.MiddleCenter
            Me.UsersMasterTile.TileTextFontWeight = MetroFramework.MetroTileTextWeight.Regular
            Me.UsersMasterTile.UseSelectable = True
            Me.UsersMasterTile.UseTileImage = True
            '
            'MenuView
            '
            Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
            Me.ClientSize = New System.Drawing.Size(800, 450)
            Me.Controls.Add(Me.UsersMasterTile)
            Me.Name = "MenuView"
            Me.Text = "MenuView"
            Me.ResumeLayout(False)

        End Sub

        Friend WithEvents UsersMasterTile As MetroFramework.Controls.MetroTile
    End Class

End Namespace