﻿Imports DST.Domain.Entities
Imports DST.Infrastructure.Oracle
Imports DST.WinForm.ViewModels
Imports MetroFramework

Namespace Views

    ''' <summary>
    ''' ユーザマスタ画面
    ''' </summary>
    Public Class UsersMasterView

        ''' <summary>
        ''' ビューモデル
        ''' </summary>
        Private ReadOnly _viewModel As New UsersMasterViewModel(New UsersOracle(New FreeContext))

        ''' <summary>
        ''' コンストラクタ
        ''' </summary>
        Public Sub New()

            ' この呼び出しはデザイナーで必要です。
            InitializeComponent()

            ' InitializeComponent() 呼び出しの後で初期化を追加します。
            UsersMasterViewModelBindingSource.DataSource = _viewModel
            UsersBindingSource.DataSource = _viewModel.UsersDataSource
            Grid.DataSource = UsersBindingSource
        End Sub

        ''' <summary>
        ''' 検索ボタンクリックイベント
        ''' </summary>
        ''' <param name="sender">コントロール</param>
        ''' <param name="e">イベント引数</param>
        Private Sub SearchButton_Click(sender As Object, e As EventArgs) Handles SearchButton.Click
            Try
                _viewModel.Search()
                UsersBindingSource.DataSource = _viewModel.UsersDataSource
            Catch ex As Exception
                HandleException(ex)
            End Try
        End Sub

        ''' <summary>
        ''' 行追加ボタンクリックイベント
        ''' </summary>
        ''' <param name="sender">コントロール</param>
        ''' <param name="e">イベント引数</param>
        Private Sub AddRowButton_Click(sender As Object, e As EventArgs) Handles AddRowButton.Click
            UsersBindingSource.Add(New UserEntity)
            UsersBindingSource.MoveLast()
        End Sub

        ''' <summary>
        ''' 保存ボタンクリックイベント
        ''' </summary>
        ''' <param name="sender">コントロール</param>
        ''' <param name="e">イベント引数</param>
        Private Sub SaveButton_Click(sender As Object, e As EventArgs) Handles SaveButton.Click
            Try
                Validate()
                _viewModel.Save()
                MetroMessageBox.Show(Me, "保存しました。", "情報", MessageBoxButtons.OK, MessageBoxIcon.Information)
            Catch ex As Exception
                HandleException(ex)
            End Try
        End Sub

    End Class

End Namespace