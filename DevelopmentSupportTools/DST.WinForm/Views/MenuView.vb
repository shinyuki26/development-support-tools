﻿Namespace Views

    ''' <summary>
    ''' メニュー画面
    ''' </summary>
    Public Class MenuView

        ''' <summary>
        ''' ユーザマスタタイルクリックイベント
        ''' </summary>
        ''' <param name="sender">コントロール</param>
        ''' <param name="e">イベント引数</param>
        Private Sub UsersMasterTile_Click(sender As Object, e As EventArgs) Handles UsersMasterTile.Click
            Dim view As New UsersMasterView
            view.Show()
        End Sub

    End Class

End Namespace