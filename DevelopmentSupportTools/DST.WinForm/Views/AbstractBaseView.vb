﻿Namespace Views

    Public Class AbstractBaseView

        ''' <summary>
        ''' 例外処理
        ''' </summary>
        ''' <param name="ex">例外</param>
        Protected Sub HandleException(ex As Exception)
            Dim icon As MessageBoxIcon = MessageBoxIcon.Error
            Dim caption As String = "エラー"
            MessageBox.Show(ex.Message, caption, MessageBoxButtons.OK, icon)
        End Sub

        Private Sub AbstractView_Load(sender As Object, e As EventArgs) Handles MyBase.Load
            StartPosition = FormStartPosition.CenterScreen
        End Sub

    End Class

End Namespace