﻿Imports DST.Domain.Entities
Imports DST.Domain.Repositories

Namespace ViewModels

    ''' <summary>
    ''' ユーザービューモデルクラス
    ''' </summary>
    Public Class UsersMasterViewModel

        ''' <summary>ユーザーレポジトリ</summary>
        Private ReadOnly _usersRepository As IUsersRepository

        ''' <summary>
        ''' コンストラクタ
        ''' </summary>
        ''' <param name="usersRepository">ユーザーレポジトリ</param>
        Public Sub New(usersRepository As IUsersRepository)
            _usersRepository = usersRepository
        End Sub

        ''' <summary>
        ''' 検索用の名前を取得または設定します。
        ''' </summary>
        ''' <returns>検索用の名前</returns>
        Public Property NameText As String = String.Empty

        ''' <summary>
        ''' ユーザーグリッド用のデータソースを取得または設定します。
        ''' </summary>
        ''' <returns>ユーザーグリッド用のデータソース</returns>
        Public Property UsersDataSource As New List(Of UserEntity)

        ''' <summary>
        ''' 検索します。
        ''' </summary>
        Public Sub Search()
            UsersDataSource = _usersRepository.GetUsersByName(NameText, True)
        End Sub

        ''' <summary>
        ''' 保存します。
        ''' </summary>
        Friend Sub Save()
            For Each user In UsersDataSource
                If user.UserId = 0 Then
                    _usersRepository.Add(user)
                End If
            Next
            _usersRepository.Commit()
        End Sub

    End Class

End Namespace