﻿Imports DST.Domain
Imports DST.Domain.Exceptions
Imports DST.Domain.Repositories

Namespace ViewModels

    ''' <summary>
    ''' ログインビューモデル
    ''' </summary>
    Public Class LoginViewModel

        ''' <summary>ユーザーレポジトリ</summary>
        Private ReadOnly _usersRepository As IUsersRepository

        ''' <summary>
        ''' コンストラクタ
        ''' </summary>
        ''' <param name="usersRepository">ユーザーレポジトリ</param>
        Public Sub New(usersRepository As IUsersRepository)
            _usersRepository = usersRepository
        End Sub

        Public Property UserIdText As String = String.Empty
        Public Property PasswordText As String = String.Empty

        Public Sub Login()
            If String.IsNullOrEmpty(UserIdText) Then
                Throw New InputException("ユーザIDを入力してください。")
            End If

            Dim userId As Integer = 0
            If Not Integer.TryParse(UserIdText, userId) Then
                Throw New InputException("ユーザIDは数値を入力してください。")
            End If

            Dim loginUser = _usersRepository.GetUserByKey(userId)
            If loginUser Is Nothing Then
                Throw New InputException("ユーザIDが利用可能ユーザとして登録されていません。")
            End If

            [Shared].LoginUser = loginUser
        End Sub

    End Class

End Namespace