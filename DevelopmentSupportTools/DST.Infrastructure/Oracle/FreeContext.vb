Imports System.Data.Entity
Imports DST.Domain.Entities

Namespace Oracle

    Partial Public Class FreeContext
        Inherits DbContext

        Public Sub New()
            MyBase.New("name=FreeContext")
            Database.SetInitializer(Of FreeContext)(Nothing)
            Database.Log = Sub(log) Console.WriteLine(log)
        End Sub

        Public Overridable Property Users As DbSet(Of UserEntity)

        Protected Overrides Sub OnModelCreating(ByVal modelBuilder As DbModelBuilder)
        End Sub

    End Class

End Namespace