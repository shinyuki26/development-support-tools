﻿Imports System.Data.Entity
Imports DST.Domain
Imports DST.Domain.Entities
Imports DST.Domain.Repositories

Namespace Oracle

    ''' <summary>
    ''' ユーザテーブルにアクセスするためのクラス。
    ''' </summary>
    Public Class UsersOracle
        Inherits AbstractOracle
        Implements IUsersRepository

        ''' <summary>
        ''' コンストラクタ
        ''' </summary>
        ''' <param name="dbContext">DBコンテキスト</param>
        Public Sub New(dbContext As FreeContext)
            MyBase.New(dbContext)
        End Sub

        ''' <inheritdoc/>
        Public Function GetUsersByName(name As String, Optional isTracking As Boolean = False) As List(Of UserEntity) Implements IUsersRepository.GetUsersByName

            Return _dbContext.Users.
                Where(Function(u) u.FamilyName.Contains(name) Or u.GivenName.Contains(name)).
                OrderBy(Function(u) u.UserId).ToList

        End Function

        ''' <inheritdoc/>
        Public Function Commit() As Integer Implements IUsersRepository.Commit
            For Each user In _dbContext.Users.Local.ToList
                Dim userState = _dbContext.Entry(user).State
                If userState = EntityState.Added Then
                    user.CreatedBy = [Shared].LoginUser.UserId
                    user.CreatedOn = Now
                    user.UpdatedBy = [Shared].LoginUser.UserId
                    user.UpdatedOn = Now
                End If
                If userState = EntityState.Modified Then
                    user.UpdatedBy = [Shared].LoginUser.UserId
                    user.UpdatedOn = Now
                End If
            Next
            Return _dbContext.SaveChanges
        End Function

        ''' <inheritdoc/>
        Public Function GetUserByKey(userId As Integer) As UserEntity Implements IUsersRepository.GetUserByKey
            Return _dbContext.Users.Find(userId)
        End Function

        ''' <inheritdoc/>
        Public Function Add(user As UserEntity) As UserEntity Implements IUsersRepository.Add
            Return _dbContext.Users.Add(user)
        End Function

    End Class

End Namespace