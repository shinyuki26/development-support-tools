﻿Namespace Oracle

    Public MustInherit Class AbstractOracle

        ''' <summary>DBコンテキスト</summary>
        Friend _dbContext As FreeContext

        ''' <summary>
        ''' コンストラクタ
        ''' </summary>
        ''' <param name="dbContext">DBコンテキスト</param>
        Public Sub New(dbContext As FreeContext)
            _dbContext = dbContext
        End Sub

    End Class

End Namespace