﻿Namespace Exceptions

    Public Class InputException
        Inherits AbstractException

        Public Sub New()
            MyBase.New("入力エラーです。")
        End Sub

        Public Sub New(message As String)
            MyBase.New(message)
        End Sub

        Public Overrides ReadOnly Property Kind As ExceptionKind
            Get
                Return ExceptionKind.Error
            End Get
        End Property

    End Class

End Namespace