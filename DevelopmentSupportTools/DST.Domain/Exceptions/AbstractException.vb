﻿Namespace Exceptions

    ''' <summary>
    ''' 基底例外クラス
    ''' </summary>
    Public MustInherit Class AbstractException
        Inherits Exception

        ''' <summary>
        ''' コンストラクタ
        ''' </summary>
        ''' <param name="message">エラーメッセージ</param>
        Public Sub New(message As String)
            MyBase.New(message)
        End Sub

        ''' <summary>
        ''' コンストラクタ
        ''' </summary>
        ''' <param name="message">エラーメッセージ</param>
        ''' <param name="exception">元になった例外</param>
        Public Sub New(message As String, exception As Exception)
            MyBase.New(message, exception)
        End Sub

        Public MustOverride ReadOnly Property Kind As ExceptionKind

        ''' <summary>
        ''' 例外種類
        ''' </summary>
        Public Enum ExceptionKind
            Info
            Warning
            [Error]
        End Enum

    End Class

End Namespace