Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports System.ComponentModel.DataAnnotations.Schema

Namespace Entities

    <Table("FREE.Users")>
    Partial Public Class UserEntity

        <DisplayName("ユーザID")>
        <Key>
        Public Property UserId As Integer

        <DisplayName("姓")>
        <StringLength(30)>
        Public Property FamilyName As String

        <DisplayName("名")>
        <StringLength(30)>
        Public Property GivenName As String

        <DisplayName("有効")>
        Public Property IsActive As Boolean = True

        <DisplayName("作成者")>
        Public Property CreatedBy As Integer

        <DisplayName("作成日時")>
        Public Property CreatedOn As Date

        <DisplayName("更新者")>
        Public Property UpdatedBy As Integer

        <DisplayName("更新日時")>
        Public Property UpdatedOn As Date

    End Class

End Namespace