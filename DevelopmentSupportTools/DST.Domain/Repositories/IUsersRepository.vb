﻿Imports DST.Domain.Entities

Namespace Repositories

    ''' <summary>
    ''' ユーザーレポジトリ
    ''' </summary>
    Public Interface IUsersRepository

        ''' <summary>
        ''' 指定されたユーザIDのユーザーを取得します。
        ''' </summary>
        ''' <param name="userId">ユーザID</param>
        ''' <returns>ユーザー</returns>
        Function GetUserByKey(userId As Integer) As UserEntity

        ''' <summary>
        ''' 指定された名前が姓または名に含まれるユーザーを取得します。
        ''' </summary>
        ''' <param name="name">名前</param>
        ''' <param name="isTracking">
        ''' Trueの場合、追跡する。
        ''' ※ 追加・変更・削除する場合のみTrueにしてください。
        ''' </param>
        ''' <returns>ユーザー</returns>
        Function GetUsersByName(name As String, Optional isTracking As Boolean = False) As List(Of UserEntity)

        ''' <summary>
        ''' 指定されたユーザを追加します。
        ''' </summary>
        ''' <param name="user">ユーザ</param>
        ''' <returns>ユーザ</returns>
        Function Add(user As UserEntity) As UserEntity

        ''' <summary>
        ''' このコンテキストで行われたすべての変更をデータベースに保存します。
        ''' </summary>
        ''' <returns>データベースに書き込まれたオブジェクトの数。</returns>
        Function Commit() As Integer

    End Interface

End Namespace