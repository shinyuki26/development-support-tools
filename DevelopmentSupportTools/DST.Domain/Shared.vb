﻿Imports DST.Domain.Entities

Public Class [Shared]

    ''' <summary>
    ''' ログインユーザ
    ''' ※ ログイン時に設定されます。
    ''' </summary>
    Public Shared Property LoginUser As UserEntity

End Class